import java.util.*;
import java.io.*;

public class MaxPairwiseProduct {
    static long getMaxPairwiseProduct(long[] numbers) {
        int n = numbers.length;
        long lastNumber=-1;
        long lastNumber2=-1;
        int index=0;
        for (int i = 0; i < n; ++i) {
		    if (lastNumber < numbers[i] || lastNumber==-1){
		    	lastNumber = numbers[i];
		    	index = i;
		    }
        }
        
        for (int i = 0; i < n; ++i) {
	    	if ((lastNumber2 < numbers[i] || lastNumber2==-1) && index!=i){
		    	lastNumber2 = numbers[i];
		    }	
        }
        return lastNumber * lastNumber2;
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n = scanner.nextInt();
        long[] numbers = new long[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = scanner.nextInt();
        }
        System.out.println(getMaxPairwiseProduct(numbers));
    }

    static class FastScanner {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(InputStream stream) {
            try {
                br = new BufferedReader(new InputStreamReader(stream));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

}