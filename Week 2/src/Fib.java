import java.util.Scanner;

public class Fib {
	 private static long calc_fib(int n) {
			long[] vec = new long[3];
			if(n<=1){	
				return n;
			}else if (n==2){
				return 1;
			}	
			else{
			
				vec[0]=0;
				vec[1]=1;
				vec[2]=1;
				for(int x=3;x<=n;x++){
					vec[0]=vec[1];
					vec[1]=vec[2];
					vec[2]=vec[0]+vec[1];
				}
			}
		    return vec[2];
		  }

		  public static void main(String args[]) {
		    Scanner in = new Scanner(System.in);
		    int n = in.nextInt();

		    System.out.println(calc_fib(n));
		  }
}