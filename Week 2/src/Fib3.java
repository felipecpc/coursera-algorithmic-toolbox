import java.math.BigInteger;
import java.util.Scanner;

public class Fib3 {
	 private static long calc_fib(long n,long m) {
			long[] vec = new long[3];
			if(n<=1){	
				return n;
			}else if (n==2){
				return 1;
			}	
			else{
			
				vec[0]=0;
				vec[1]=1;
				vec[2]=1;
				for(int x=3;x<=n;x++){
					vec[0]=vec[1];
					vec[1]=vec[2];
					vec[2]=(vec[0]+vec[1]);
				}
			}
		    return vec[2];
		  }

		  public static void main(String args[]) {
		    Scanner in = new Scanner(System.in);
		    long n = in.nextLong();
		    long m = in.nextLong();

		    System.out.println(calc_fib(n,m));
		  }
}