import java.math.BigInteger;
import java.util.Scanner;

public class Fib4 {
	 private static long calc_fib(long n,long m) {
			long[] vec = new long[3];
			boolean flag=false;
			long period=0;
			if(n<=1){	
				return n;
			}else if (n==2){
				return 1;
			}	
			else{
			
				vec[0]=0;
				vec[1]=1;
				vec[2]=1;
				for(int x=3;;x++){
					//System.out.println("cont = " + x);
					vec[0]=vec[1];
					vec[1]=vec[2];
					vec[2]=(vec[0]+vec[1])%m;
					
					if(vec[2]==0){
						flag=true;
					}else if(vec[2]==1 && flag==true){
						period=x-1;
						break;
					}else{
						flag=false;
					}
				}
			}
			//System.out.println("Period = " + period);
			long num = n % period;
			//System.out.println("num = " + num + " fib="+calc_fib2(num,m));
		    return (calc_fib2(num,m));
		  }

	 private static long calc_fib2(long n,long m) {
			long[] vec = new long[3];
			if(n<=1){	
				return n;
			}else if (n==2){
				return 1;
			}	
			else{
			
				vec[0]=0;
				vec[1]=1;
				vec[2]=1;
				for(int x=3;x<=n;x++){
					vec[0]=vec[1];
					vec[1]=vec[2];
					vec[2]=(vec[0]+vec[1])%m;
				}
			}
		    return vec[2];
		  }

	 
		  public static void main(String args[]) {
		    Scanner in = new Scanner(System.in);
		    long n = in.nextLong();
		    long m = in.nextLong();

		    System.out.println(calc_fib(n,m));
		  }
}