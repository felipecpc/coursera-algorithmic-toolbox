import java.util.Scanner;

public class gdc {
  private static long gcd(long a, long b) {
	long r=1,res=0;
	
	while(r!=0){
		if(a>b){
			r = b;
			b = a % b;
			a=r;
			r=b;
			res =a;
		}else{
			r=a;
			a = b % a;
			b=r;
			r=a;
			res=b;
		}
	}

    return res;
  }

  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    long a = scanner.nextLong();
    long b = scanner.nextLong();

    System.out.println(gcd(a, b));
  }
}
