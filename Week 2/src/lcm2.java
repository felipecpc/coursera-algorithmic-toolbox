import java.util.Scanner;

public class lcm2 {
  private static long lcm2(long a, long b) {
	long r=1,res=0;
	long aa=a,bb=b;
	
	while(r!=0){
		if(a>b){
			r = b;
			b = a % b;
			a=r;
			r=b;
			res =a;
		}else{
			r=a;
			a = b % a;
			b=r;
			r=a;
			res=b;
		}
	}

    return (aa*bb)/res;
  }

  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    long a = scanner.nextLong();
    long b = scanner.nextLong();

    System.out.println(lcm2(a, b));
  }
}
