import java.util.Scanner;

public class Change {
    private static int getChange(int n) {
    	int value=0,cont=0;
    	while(value<n){
    		if((value + 10)<=n){
    			value = value +10;
    		}
    		else if((value + 5)<=n){
    			value = value +5;
    		}
    		else {
    			value++;
    		}
    		cont++;
    	}
        //write your code here
        return cont;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(getChange(n));

    }
}
