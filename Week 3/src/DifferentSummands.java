import java.util.*;

public class DifferentSummands {
    private static List<Integer> optimalSummands(int n) {
        List<Integer> summands = new ArrayList<Integer>();
        
        int k=n,l=1;
        
        while(k!=0){
        	if(k<=(2*l)){
        		summands.add(k);
        		k=0;
        	}else{
        		k = k-l;
        		summands.add(l);
        		l++;
        	}
        }
        //write your code here
        return summands;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> summands = optimalSummands(n);
        System.out.println(summands.size());
        for (Integer summand : summands) {
            System.out.print(summand + " ");
        }
    }
}

