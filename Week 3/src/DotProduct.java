import java.util.*;

public class DotProduct {
    private static long minDotProduct(int[] a, int[] b) {
        //write your code here
        long result = 0, result2=0;
        
        Arrays.sort(a);
        Arrays.sort(b);
        for (int i = 0; i < a.length; i++) {
        	//System.out.println("a="+a[i]+ " b="+ b[a.length-1-i] + " a*b=" + (a[i] * b[a.length-1-i]));
            result += (long)a[i] * (long)b[a.length-1-i];
            result2 +=(long)a[a.length-1-i] * (long)b[i];
        }
        
        if(result2<result) return result2;
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        System.out.println(minDotProduct(a, b));
    }
}

