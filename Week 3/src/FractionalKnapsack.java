import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Scanner;

public class FractionalKnapsack {
	
	
    private static double getOptimalValue(int capacity, int[] values, int[] weights) {
        double value = 0;
        int[] A = new int[values.length];
        for(int x=0;x<values.length;x++){
        	A[x]=0;
        	//System.out.println(" => values " + values[x] + " weights " + weights[x]);
        }
        int index=0;
        
        for(int x=0;x<values.length;x++){
        	if(capacity==0){
        		return value;
        		
        		
        	}else{
        		index=getMax(values,weights,A);
        		A[index]=1;
        		//System.out.println("--> index = " + index);
        		if(capacity<weights[index]){
        			value = value + capacity*((double)values[index]/(double)weights[index]);
        			
        			capacity = 0;
        		}else{
        			
        			value = value + values[index];
        			capacity = capacity - weights[index];
        		}
        	}
        }
        

        
        DecimalFormat decim = new DecimalFormat("#.0000");


        return Double.parseDouble(decim.format(value).replace(',','.'));
    }
    

    
    public static int getMax(int[] values,int[] weights, int[] A){
    	
    	int ret=0;
    	double max=0,temp=0;
    	for(int x=0;x<values.length;x++){
    		temp = (double)values[x]/(double)weights[x];
    		//System.out.println("Max= "+max + " temp=" + temp);
    		if(max<temp && A[x]==0){
    			ret = x;
    			max=temp;
    		}
    	}
    	return ret;
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int capacity = scanner.nextInt();
        int[] values = new int[n];
        int[] weights = new int[n];
        for (int i = 0; i < n; i++) {
            values[i] = scanner.nextInt();
            weights[i] = scanner.nextInt();
        }

        /*int n = 3;
        int capacity = 50;
        int[] values = {60,100,120};
        int[] weights = {20,50,30};*/

        System.out.printf("%.4f",getOptimalValue(capacity, values, weights));
    }
} 
